;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Kenneth Flak"
      user-mail-address "kennethflak@protonmail.com"
      )
(setq mail-user-agent 'message-user-agent)
(setq
      message-send-mail-function 'smtpmail-send-it
      smtpmail-stream-type 'starttls
      smtpmail-smtp-server "127.0.0.1"
      smtpmail-smtp-service 587
      )

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;;
;; accept. For example:
;;
(setq doom-font (font-spec :family "Fira Code" :size 20))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;               ;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

; supercollider
(add-to-list 'load-path "/usr/bin/sclang")
(add-to-list 'load-path "/usr/share/emacs/site-lisp/SuperCollider")
(require 'sclang)

                                        ; "Enable eldoc-mode for sclang"
(defun sclang-turn-on-eldoc-mode ()
  (interactive)
  (set (make-local-variable 'eldoc-documentation-function)
       'sclang-show-method-args)
  (turn-on-eldoc-mode))
(add-hook 'sclang-mode-hook 'sclang-turn-on-eldoc-mode)

; momentary-highlight
(defun yurb/sclang-highlight-defun (&optional silent-p)
  (cl-multiple-value-bind (beg end) (sclang-point-in-defun-p)
    (and beg end (pulse-momentary-highlight-region beg end))))

(defun yurb/sclang-highlight-region-or-line (&optional silent-p)
  (if (and transient-mark-mode mark-active)
      (pulse-momentary-highlight-region (region-beginning) (region-end))
    (pulse-momentary-highlight-one-line (point))))

(advice-add 'sclang-eval-defun :before #'yurb/sclang-highlight-defun)
(advice-add 'sclang-eval-region-or-line :before #'yurb/sclang-highlight-region-or-line)

(map! :leader
      (:prefix("k" . "SuperCollider")
       :ne
       :desc "Start SCLang"
       "s" (cmd!
            (sclang-start)
            (+evil/window-move-left))))

;; (map! :after 'sclang
;;       :mode 'sclang-mode
;;       :n
;;       :map sclang-mode-map
;;       :desc "Evaluate region or line"
;;       "SPC" #'sclang-eval-region-or-line)

;; use clangd, not ccls
(setq lsp-clients-clangd-args '("-j=3"
				"--background-index"
				"--clang-tidy"
				"--completion-style=detailed"
				"--header-insertion=never"
				"--header-insertion-decorators=0"))
(after! lsp-clangd (set-lsp-priority! 'clangd 2))

;; calendar
;; (defun my-open-calendar ()
;;   (interactive)
;;   (cfw:open-calendar-buffer
;;    :contents-sources
;;    (list
;;     (cfw:ical-create-source "Calendar" "~/.calendars/woelkli/kennethflakgmair.com" "Gray")

;; (setq calendar-week-start-day 1)
(setq doom-leader-key "SPC"
      doom-localleader-key "\\"
      projectile-project-search-path '("~/.local/share/SuperCollider/Extensions" "~/sc" "~/bin" "~/dots" "~/html" "~/Documents/singularityOffice" "~/Documents/singularityApplications")
      )

(require 'emms-setup)
(emms-all)
(emms-default-players)
(setq emms-source-file-default-directory "~/Documents/Music/")

(setq +notmuch-home-function (lambda () (notmuch-search "tag:inbox")))

; open url in external browser
(defun w3mext-open-link-or-image-or-url ()
  (interactive)
  (let (url)
    (if (string= major-mode "w3m-mode")
        (setq url (or (w3m-anchor) (w3m-image) w3m-current-url)))
    (browse-url-generic (if url url (car (browse-url-interactive-arg "URL: "))))
    ))
(global-set-key (kbd "C-c b") 'w3mext-open-link-or-image-or-url)
(setq browse-url-generic-program "qutebrowser")

(setq evil-respect-visual-line-mode 1)

(xclip-mode 1)

; autosave all the time
(defun full-auto-save ()
  (interactive)
  (save-excursion
    (dolist (buf (buffer-list))
      (set-buffer buf)
      (if (and (buffer-file-name) (buffer-modified-p))
          (basic-save-buffer)))))
(add-hook 'auto-save-hook 'full-auto-save)

(defun save-all ()
  (interactive)
  (save-some-buffers t))
  (add-hook 'after-focus-change-function 'save-all)

; not quite vim elegance, unfortunately... better than nothing
(defun my-increment-number-decimal (&optional arg)
  "Increment the number forward from point by 'arg'."
  (interactive "p*")
  (save-excursion
    (save-match-data
      (let (inc-by field-width answer)
        (setq inc-by (if arg arg 1))
        (skip-chars-backward "0123456789")
        (when (re-search-forward "[0-9]+" nil t)
          (setq field-width (- (match-end 0) (match-beginning 0)))
          (setq answer (+ (string-to-number (match-string 0) 10) inc-by))
          (when (< answer 0)
            (setq answer (+ (expt 10 field-width) answer)))
          (replace-match (format (concat "%0" (int-to-string field-width) "d") answer)))))))

(defun my-decrement-number-decimal (&optional arg)
  (interactive "p*")
  (my-increment-number-decimal (if arg (- arg) -1)))

(global-set-key (kbd "C-c a") 'my-increment-number-decimal)
(global-set-key (kbd "C-c x") 'my-decrement-number-decimal)

; fzf
(global-set-key (kbd "C-c g") 'fuzzy-finder-goto-gitgrep-line)
(global-set-key (kbd "C-c f") 'fuzzy-finder-find-files-projectile)
